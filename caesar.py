from tkinter import * 
frame = Tk() 

class Interface(Frame):

    def __init__(self, affiche, **kwargs):
        Frame.__init__(self, affiche, **kwargs)

        self.pack(fill=BOTH)
        self.TitreApp = Label(self, text="Nom de code : Caesar")        #Le titre
        self.TitreApp.pack()

        self.MsgCoderlabel = Label(self, text="Coder : ")               
        self.MsgCoderlabel.pack()

        self.MsgACoder = Entry(self)
        self.MsgACoder.insert(0, "Saisir un message")                   #Affichage pour saisie du message
        self.MsgACoder.pack()

        self.Clef = Entry(self)
        self.Clef.insert(0, "Clef")                                     #Affichage pour saisie de la clef
        self.Clef.pack()

        self.BtnCoder = Button(self, text="Coder", command= lambda : self.Codage(str(self.MsgACoder.get()), int(self.Clef.get())))         #bouton de validation
        self.BtnCoder.pack()

        self.ResultatCode = Label(self, text = "")
        self.ResultatCode.pack()

        self.MsgDecoderlabel = Label(self, text="Decoder : ")
        self.MsgDecoderlabel.pack()

        self.MsgADecoder = Entry(self)
        self.MsgADecoder.insert(0, "Saisir un message code")        #Affichage pour saisie du message codé
        self.MsgADecoder.pack()


        self.ClefPourDecoder = Entry(self)
        self.ClefPourDecoder.insert(0, "Clef")                      #Affichage pour saisie de la clef
        self.ClefPourDecoder.pack()

        self.BtnDecoder = Button(self, text="Decoder", command=self.Decodage)        #Bouton de validation
        self.BtnDecoder.pack()
                
        self.Resultat = Label(self, text = "")
        self.Resultat.pack()



    def Codage(self, Phrase, Clef):                                   #Fonction de codage
        if self.Resultat['text'] != "":
            self.Resultat.config(text = "")

        phraseBrut = Phrase
        TaillePhrase = len(Phrase)

        cle = Clef

        PhraseCodee = ""                        #Vide pour éviter la concatenation

        for i in range(0, TaillePhrase):
            var = ord(Phrase[i])                
            var += Clef                         #Ajoute la clef au caractere contenu dans "Phrase[i]"

            var =  chr(var)
            PhraseCodee += var                  #Ajoute la phrase coder a notre variable vidée juste avant

        self.ResultatCode.config(text = str(PhraseCodee))

        self.ResetCodeEntry()



    def Decodage(self):                         #Fonction de decodage

        if self.ResultatCode['text'] != "":
            self.ResultatCode.config(text = "")


        PhraseChiffree = str(self.MsgADecoder.get())    #recupere la phrase
        TaillePhraseChiffree = len(PhraseChiffree)      #recupere la taille de la phrase

        cle = int(self.ClefPourDecoder.get())

        PhraseDecodee = ""                              #Vide pour éviter la concatenation

        for i in range(0, TaillePhraseChiffree):
            var = ord(PhraseChiffree[i])
            var -= cle                  #Retire la valeur de la clef au caractere contenu dans "PhraseChiffree[i]"

            var = chr(var)
            PhraseDecodee += var        #Ajoute la phrase décoder a notre variable vidée juste avant
        
        self.Resultat.config(text = str(PhraseDecodee))
        self.ResetDecodeEntry()

interface = Interface(frame)
interface.mainloop()